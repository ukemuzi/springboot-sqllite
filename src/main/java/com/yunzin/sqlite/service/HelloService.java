package com.yunzin.sqlite.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunzin.sqlite.entity.HelloEntity;

public interface HelloService extends IService<HelloEntity> {

}