package com.yunzin.sqlite.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/face")
public class FaceResultController {

    @GetMapping("/faceResult")
    public String demo(HttpServletRequest request, HttpServletResponse response) {

        return "faceResult";
    }
}
