package com.yunzin.sqlite.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunzin.sqlite.entity.HelloEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
public interface HelloDao extends BaseMapper<HelloEntity> {

}