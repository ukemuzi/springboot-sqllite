# springboot-sqllite

#### 介绍
SpringBoot集成SQLite示例，可以直接拿来改造即可应用到项目中，项目中附带集成了Pear Admin Layui前端，便于快速集成使用，用于简单的应用系统和小数据量业务场景。

#### 软件架构
1.  springboot2.2.6.RELEASE
2.  MyBatis-plus 3
3.  SQLite 3


#### 使用步骤

1.  git clone 仓库代码
2.  使用Idea工具打开编译
3.  使用tomcat启动
![输入图片说明](https://images.gitee.com/uploads/images/2021/0324/082059_eef64734_2515.png "屏幕截图.png")

4.  Pear Admin Layui样式效果图
![输入图片说明](https://cdn.nlark.com/yuque/0/2021/png/1363239/1617756928803-f391c174-56f7-41b8-b39a-2dd02310bc25.png?x-oss-process=image%2Fresize%2Cw_1147%2Climit_0)
![输入图片说明](https://cdn.nlark.com/yuque/0/2021/png/1363239/1617756942900-3aa04d41-ef0c-4c63-ab7b-316265118067.png?x-oss-process=image%2Fresize%2Cw_1147%2Climit_0)
![输入图片说明](https://cdn.nlark.com/yuque/0/2021/png/1363239/1617756946992-ddd673c6-f118-43f9-97a4-aaa657eaf5cc.png?x-oss-process=image%2Fresize%2Cw_1147%2Climit_0)
![输入图片说明](https://cdn.nlark.com/yuque/0/2021/png/1363239/1617756963372-f26e8b68-3c21-4688-a638-27207ce63af2.png?x-oss-process=image%2Fresize%2Cw_1147%2Climit_0)
![输入图片说明](https://cdn.nlark.com/yuque/0/2021/png/1363239/1617756972353-0841b067-ff78-4327-afd8-e382c04eb495.png?x-oss-process=image%2Fresize%2Cw_1147%2Climit_0)

#### 友情链接
[@Pear Admin Layui](https://gitee.com/pear-admin/Pear-Admin-Layui)